"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dateStringToDate = void 0;
var dateStringToDate = function (date) {
    var _a = date.split('/'), day = _a[0], month = _a[1], year = _a[2];
    return new Date(+year, +month, +day);
};
exports.dateStringToDate = dateStringToDate;

import { CsvFileReader } from './CsvFileReader'
import { MatchResult } from '../MatchResult'
import { dateStringToDate } from '../utils'

type MatchData = [Date, string, string, number, number, MatchResult, string]

export class MatchReader extends CsvFileReader<MatchData> {
    mapRow ([
                date,
                homeTeam,
                AwayTeam,
                homeGoals,
                awayGoals,
                result,
                arbitrary
            ]: string[]): MatchData {
        return [
            dateStringToDate(date),
            homeTeam,
            AwayTeam,
            parseInt(homeGoals),
            parseInt(awayGoals),
            result as MatchResult,
            arbitrary
        ]
    }
}

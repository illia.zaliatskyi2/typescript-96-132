import { MatchData } from './MatchData'
import { WinAnalysis } from './analyzers/WinAnalysis'
import { HtmlReport } from './reportTargets/HtmlReport'

export interface Analyzer {
    run(matched: MatchData[]): string
}

export interface  OutputTarget {
    print(report: string): void
}

export class Summary {
    static winsAnalysisHtmlReport(team: string) {
        return new Summary(
            new WinAnalysis(team),
            new HtmlReport()
        )
    }

    constructor(
        public analyzer: Analyzer,
        public outputTarget: OutputTarget
    ) {}

    buildAndPrintReport(matches: MatchData[]): void {
        const report = this.analyzer.run(matches)

        this.outputTarget.print(report)
    }
}
